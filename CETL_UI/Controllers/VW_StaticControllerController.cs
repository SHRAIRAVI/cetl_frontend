﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CETL_UI.Models;

namespace CETL_UI.Controllers
{
    public class VW_StaticControllerController : Controller
    {
        private MDSEntities db = new MDSEntities();

        // GET: VW_StaticController
        public ActionResult Index()
        {
            
            return View(db.VW_StaticController.ToList());
        }

        // GET: VW_StaticController/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VW_StaticController vW_StaticController = db.VW_StaticController.Find(id);
            if (vW_StaticController == null)
            {
                return HttpNotFound();
            }
            return View(vW_StaticController);
        }

        // GET: VW_StaticController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: VW_StaticController/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,MUID,VersionName,VersionNumber,Version_ID,VersionFlag,Name,Code,ChangeTrackingMask,ControlMasterID,EnterDateTime,EnterUserName,EnterVersionNumber,LastChgDateTime,LastChgUserName,LastChgVersionNumber,ValidationStatus")] VW_StaticController vW_StaticController)
        {
            if (ModelState.IsValid)
            {
                db.VW_StaticController.Add(vW_StaticController);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(vW_StaticController);
        }

        // GET: VW_StaticController/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VW_StaticController vW_StaticController = db.VW_StaticController.Find(id);
            if (vW_StaticController == null)
            {
                return HttpNotFound();
            }
            return View(vW_StaticController);
        }

        // POST: VW_StaticController/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,MUID,VersionName,VersionNumber,Version_ID,VersionFlag,Name,Code,ChangeTrackingMask,ControlMasterID,EnterDateTime,EnterUserName,EnterVersionNumber,LastChgDateTime,LastChgUserName,LastChgVersionNumber,ValidationStatus")] VW_StaticController vW_StaticController)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vW_StaticController).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(vW_StaticController);
        }

        // GET: VW_StaticController/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VW_StaticController vW_StaticController = db.VW_StaticController.Find(id);
            if (vW_StaticController == null)
            {
                return HttpNotFound();
            }
            return View(vW_StaticController);
        }

        // POST: VW_StaticController/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            VW_StaticController vW_StaticController = db.VW_StaticController.Find(id);
            db.VW_StaticController.Remove(vW_StaticController);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
