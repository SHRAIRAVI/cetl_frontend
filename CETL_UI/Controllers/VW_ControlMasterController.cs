﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CETL_UI.Models;
using Excel = Microsoft.Office.Interop.Excel; //change

namespace CETL_UI.Controllers
{
    public class VW_ControlMasterController : Controller
    {
        private MDSEntities db = new MDSEntities();
        private List<VW_ControlMaster> cntrolMasterList = null;

        // GET: VW_ControlMaster
        private void IndexLoadData()
        {
            var cntrolMaster = db.VW_ControlMaster;

            cntrolMasterList = new List<VW_ControlMaster>();

            foreach(var item in cntrolMaster)
            {
                item.drpdwnValues = db.VW_StaticController
                     .Where(m => m.ControlMasterID == item.ID.ToString()).Select(m => m.Name).ToList();

                cntrolMasterList.Add(item);
            }
        }

        // GET: VW_ControlMaster
        public ActionResult Index()
        {
            IndexLoadData();

            return View(cntrolMasterList);
        }

        // GET: VW_ControlMaster/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VW_ControlMaster vW_ControlMaster = db.VW_ControlMaster.Find(id);
            if (vW_ControlMaster == null)
            {
                return HttpNotFound();
            }
            return View(vW_ControlMaster);
        }

        // GET: VW_ControlMaster/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: VW_ControlMaster/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,MUID,VersionName,VersionNumber,Version_ID,VersionFlag,Name,Code,ChangeTrackingMask,LabelName,ControlName_Code,ControlName_Name,ControlName_ID,DataType_Code,DataType_Name,DataType_ID,isRequired_Code,isRequired_Name,isRequired_ID,EnterDateTime,EnterUserName,EnterVersionNumber,LastChgDateTime,LastChgUserName,LastChgVersionNumber,ValidationStatus")] VW_ControlMaster vW_ControlMaster)
        {
            if (ModelState.IsValid)
            {
                db.VW_ControlMaster.Add(vW_ControlMaster);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(vW_ControlMaster);
        }

        // GET: VW_ControlMaster/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VW_ControlMaster vW_ControlMaster = db.VW_ControlMaster.Find(id);
            if (vW_ControlMaster == null)
            {
                return HttpNotFound();
            }
            return View(vW_ControlMaster);
        }

        // POST: VW_ControlMaster/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,MUID,VersionName,VersionNumber,Version_ID,VersionFlag,Name,Code,ChangeTrackingMask,LabelName,ControlName_Code,ControlName_Name,ControlName_ID,DataType_Code,DataType_Name,DataType_ID,isRequired_Code,isRequired_Name,isRequired_ID,EnterDateTime,EnterUserName,EnterVersionNumber,LastChgDateTime,LastChgUserName,LastChgVersionNumber,ValidationStatus")] VW_ControlMaster vW_ControlMaster)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vW_ControlMaster).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(vW_ControlMaster);
        }

        // GET: VW_ControlMaster/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VW_ControlMaster vW_ControlMaster = db.VW_ControlMaster.Find(id);
            if (vW_ControlMaster == null)
            {
                return HttpNotFound();
            }
            return View(vW_ControlMaster);
        }

        // POST: VW_ControlMaster/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            VW_ControlMaster vW_ControlMaster = db.VW_ControlMaster.Find(id);
            db.VW_ControlMaster.Remove(vW_ControlMaster);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult Clear()
        {

            ModelState.Clear();
            IndexLoadData();
            return View("Index",cntrolMasterList);
            
        }


        

        //added function
        [HttpPost]
        public ActionResult Import(HttpPostedFileBase excelfile)
        {
            if (excelfile == null || excelfile.ContentLength == 0)
            {
                ViewBag.Error = "Please select a excel file<br>";
                
                if (cntrolMasterList != null)
                    return View(cntrolMasterList);
                else
                {
                    IndexLoadData();
                    return View("Index", cntrolMasterList);
                }
            }
            else
            {
                if (excelfile.FileName.EndsWith("xls") || excelfile.FileName.EndsWith("xlsx"))
                {
                    string path = Server.MapPath("~/Content" + excelfile.FileName);
                    if (System.IO.File.Exists(path))
                        System.IO.File.Delete(path);
                    excelfile.SaveAs(path);
                    Excel.Application application = new Excel.Application();
                    Excel.Workbook workbook = application.Workbooks.Open(path);
                    Excel.Worksheet worksheet = workbook.ActiveSheet;
                    Excel.Range range = worksheet.UsedRange;
                    List<GridView> listgridViews = new List<GridView>();
                    for (int row = 1; row <= range.Rows.Count ; row++)
                    {
                        GridView g = new GridView();
                        g.TagName = ((Excel.Range)range.Cells[row, 1]).Text;
                        g.TagValue = ((Excel.Range)range.Cells[row, 2]).Text;
                        g.TagTimestamp = ((Excel.Range)range.Cells[row, 3]).Text;
                      
                        listgridViews.Add(g);
                    }
                    ViewBag.ListGridViews = listgridViews;
                    if (cntrolMasterList != null)
                        return View(cntrolMasterList);
                    else
                    {
                        IndexLoadData();
                        return View("Index", cntrolMasterList);
                    }


                }
                else
                {
                    ViewBag.Error = "File type is incorrect<br>";
                    if (cntrolMasterList != null)
                        return View(cntrolMasterList);
                    else
                    {
                        IndexLoadData();
                        return View("Index", cntrolMasterList);
                    }
                }

            }



        }


    }
}
