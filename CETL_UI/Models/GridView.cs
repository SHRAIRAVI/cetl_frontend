﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;



namespace CETL_UI.Models
{
    public class GridView
    {
        public string TagName { get; set; }
        public string TagValue { get; set; }
        public string TagTimestamp { get; set; }
        
    }
}