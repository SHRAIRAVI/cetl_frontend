﻿var rowNum;
var rowNumCopy;
$(document).ready(function () {
    $("#gridv").on("click", ".btn-link", function (event) {   //fetch grid enable fields function
        rowNum = $(this).index();
        $("#txt1").removeAttr("disabled");
        $("#txt2").removeAttr("disabled");
        $("#txt3").removeAttr("disabled");
    });



    $("#gridv tbody ").on("click", "tr", function (event) {   //fetch grid with disabled fields function
        rowNum = $(this).index();
        rowNumCopy = rowNum;
        var currentRow = $(this).closest("tr");
        var col1 = currentRow.find("td:eq(0)").html();// get current row 1st TD value
        var col2 = currentRow.find("td:eq(1)").html(); // get current row 2nd TD
        var col3 = currentRow.find("td:eq(2)").html();
      
        $("#txt1").val(col1);
        $("#txt2").val(col2);
        $("#txt3").val(col3);


        $("#txt1").attr("disabled", "disabled");
        $("#txt2").attr("disabled", "disabled");
        $("#txt3").attr("disabled", "disabled");



    });






    $("#res1").on("click", function (event) {      ///clear function

        $("#txt1").removeAttr("disabled");
        $("#txt2").removeAttr("disabled");
        $("#txt3").removeAttr("disabled");


        $("#txt1").val("");
        $("#txt2").val("");
        $("#txt3").val("");



    });





    $("#add1").on("click", function (event) {      ///add function

        if ($("#gridv tr").length <= 0) {
            alert("Import Failed or Empty fields");

        }
        else {

            var tagName = $("#txt1").val();
            var tagValue = $("#txt2").val();
            var tagTimestamp = $("#txt3").val();
            var markup = "<tr><td>" + tagName + "</td><td>" + tagValue + "</td><td>" + tagTimestamp + "</td><td><button type='button' id='editDetails1'  class='btn-link'>Edit</button></td></tr>";
            $("#gridv tbody").append(markup);
            alert("Record Added Succesfully");
            $("#txt1").val("");
            $("#txt2").val("");
            $("#txt3").val("");


        }
    });



    $("#upd1").on("click", function (event) {      ///update function
        var NULL = null;
        if (($("#gridv tr").length <= 0) || (NULL == ($("#txt1").val())) || (NULL == ($("#txt2").val())) || (NULL == ($("#txt1").val())) || (NULL == rowNumCopy)) {     //checksfor textboxes & table & rowexist
            alert("Import Failed or Empty fields");
        }

        else {

            var dataTable = $("#gridv");
            var tagName = $("#txt1").val();
            var tagValue = $("#txt2").val();
            var tagTimestamp = $("#txt3").val();

            dataTable[0].rows[rowNumCopy + 1].cells[0].innerHTML = tagName;
            dataTable[0].rows[rowNumCopy + 1].cells[1].innerHTML = tagValue;
            dataTable[0].rows[rowNumCopy + 1].cells[2].innerHTML = tagTimestamp;
            rowNumCopy = null;

            var markup = "Record updated in row:" + rowNumCopy + "\n" + tagName + "\n" + tagValue + "\n" + tagTimestamp;
            $("#txt1").val("");
            $("#txt2").val("");
            $("#txt3").val("");
            alert(markup);



        }




    });
});